# -*- coding: utf-8 -*-
"""
Created on Mon Feb  4 14:46:07 2019

@author: Student
"""

import pygame as pg
import math

#draw a circle given the center of a spot
def drawCircle(pg, screen, x, y):
    pg.draw.circle(screen, [0,0,255], [x, y], 50, 4)

#draw an 'X' given the center of a spot    
def drawX(pg, screen, x, y):
    pg.draw.line(screen, [0,255,0], [x - 50 ,y - 50], [x + 50,y + 50], 6)
    pg.draw.line(screen, [0,255,0], [x - 50 ,y + 50], [x + 50,y - 50], 6)
    
# new functions for drawing faded hovers
#draw a hover circle given the center of a spot
def drawHCircle(pg, screen, x, y):
    pg.draw.circle(screen, [200,200,255], [x, y], 50, 3)

#draw a hover 'X' given the center of a spot    
def drawHX(pg, screen, x, y):
    pg.draw.line(screen, [200,255,200], [x - 50 ,y - 50], [x + 50,y + 50], 5)
    pg.draw.line(screen, [200,255,200], [x - 50 ,y + 50], [x + 50,y - 50], 5)
    
    
# new function may 16
def fillSpot(pg, screen, x, y):
    pg.draw.rect(screen, [255,255,255], pg.Rect(x-100, y-100, 200, 200), 0)

#main function
def main():
    
    #turnSpace = pg.Rect(360, 170, 25, 100)
    #swap turn
    def turnSwap(turn):
        if turn == 1:
            font = pg.font.SysFont('Calibri', 25, False, False)
            pg.draw.rect(screen, [255,255,255], pg.Rect(440, 170, 100, 25), 0)
            text = font.render("Player 2's turn", True, [0,0,0])
            screen.blit(text, [440, 170])
            turn = 2
        elif turn == 2:
            font = pg.font.SysFont('Calibri', 25, False, False)
            pg.draw.rect(screen, [255,255,255], pg.Rect(440, 170, 100, 25), 0)
            text = font.render("Player 1's turn", True, [0,0,0])
            screen.blit(text, [440, 170])
            turn = 1
        print("turn is ", turn)
        return turn
    
    def checkwin():
    # x win conditions
        if occupied[0] == 1 and occupied[1] == 1 and occupied[2] == 1:
            return 2
        if occupied[3] == 1 and occupied[4] == 1 and occupied[5] == 1:
            return 2
        if occupied[6] == 1 and occupied[7] == 1 and occupied[8] == 1:
            return 2
        if occupied[0] == 1 and occupied[3] == 1 and occupied[6] == 1:
            return 2
        if occupied[1] == 1 and occupied[4] == 1 and occupied[7] == 1:
            return 2
        if occupied[2] == 1 and occupied[5] == 1 and occupied[8] == 1:
            return 2
        if occupied[0] == 1 and occupied[4] == 1 and occupied[8] == 1:
            return 2
        if occupied[2] == 1 and occupied[4] == 1 and occupied[6] == 1:
            return 2
        # O win conditions    
        if occupied[0] == 2 and occupied[1] == 2 and occupied[2] == 2:
            return 3
        if occupied[3] == 2 and occupied[4] == 2 and occupied[5] == 2:
            return 3
        if occupied[6] == 2 and occupied[7] == 2 and occupied[8] == 2:
            return 3
        if occupied[0] == 2 and occupied[3] == 2 and occupied[6] == 2:
            return 3
        if occupied[1] == 2 and occupied[4] == 2 and occupied[7] == 2:
            return 3
        if occupied[2] == 2 and occupied[5] == 2 and occupied[8] == 2:
            return 3
        if occupied[0] == 2 and occupied[4] == 2 and occupied[8] == 2:
            return 3
        if occupied[2] == 2 and occupied[4] == 2 and occupied[6] == 2:
            return 3
        #tied game
        tied = 0
        for i in range(9):
            if occupied[i] != 0:
                tied += 1
        if tied == 9:
            return 1
        else:
            return 0
    pg.init()
    screen = pg.display.set_mode([600,800])
    clock = pg.time.Clock()
    bg_color = [255,255,255]
    done = False
    screen.fill(bg_color) 
    #add the title
    font = pg.font.SysFont('Calibri', 95, False, False)
    text = font.render("Bic Bac Boe", True, [0,0,0])
    screen.blit(text, [100, 40])
    font = pg.font.SysFont('Calibri', 25, False, False)
    pg.draw.rect(screen, [255,255,255], pg.Rect(440, 170, 100, 25), 0)
    text = font.render("Player 1's turn", True, [0,0,0])
    screen.blit(text, [440, 170])
            
    turn = 1
    # 0 is un occupied, 1 is x, 2 is o
    occupied = [0,0,0,0,0,0,0,0,0]
    played = False
    winner = False
    pauseGame = False
    while not done:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                done = True
            elif event.type == pg.MOUSEBUTTONDOWN and event.button == 1 and pauseGame == False:
                mouse_pos = event.pos
                #get x and y references so that we aren't referencing the array each time
                clickX = mouse_pos[0]
                clickY = mouse_pos[1]
                #spot 1
                if clickX > 0 and clickX < 200 and clickY > 200 and clickY < 400 and occupied[0] == 0:
                    played = True
                    if turn == 1:
                        drawX(pg, screen, 100, 300)
                        occupied[0] = 1
                        winner = True
                    elif turn == 2:
                        drawCircle(pg, screen, 100, 300)
                        occupied[0] = 2
                        winner = True
                #spot 2
                if clickX > 200 and clickX < 400 and clickY > 200 and clickY < 400 and occupied[1] == 0:
                    played = True
                    if turn == 1:
                        drawX(pg, screen, 300, 300)
                        occupied[1] = 1
                        winner = True
                    elif turn == 2:
                        drawCircle(pg, screen, 300, 300)
                        occupied[1] = 2
                        winner = True
                #spot 3
                if clickX > 400 and clickX < 600 and clickY > 200 and clickY < 400 and occupied[2] == 0:
                    played = True
                    if turn == 1:
                        drawX(pg, screen, 500, 300)
                        occupied[2] = 1
                        winner = True
                    elif turn == 2:
                        drawCircle(pg, screen, 500, 300)
                        occupied[2] = 2
                        winner = True
                #spot 4
                if clickX > 0 and clickX < 200 and clickY > 400 and clickY < 600 and occupied[3] == 0:
                    played = True
                    if turn == 1:
                        drawX(pg, screen, 100, 500)
                        occupied[3] = 1
                        winner = True
                    elif turn == 2:
                        drawCircle(pg, screen, 100, 500)
                        occupied[3] = 2
                        winner = True
                #spot 5
                if clickX > 200 and clickX < 400 and clickY > 400 and clickY < 600 and occupied[4] == 0:
                    played = True
                    if turn == 1:
                        drawX(pg, screen, 300, 500)
                        occupied[4] = 1
                        winner = True
                    elif turn == 2:
                        drawCircle(pg, screen, 300, 500)
                        occupied[4] = 2
                        winner = True
                #spot 6
                if clickX > 400 and clickX < 600 and clickY > 400 and clickY < 600 and occupied[5] == 0:
                    played = True
                    if turn == 1:
                        drawX(pg, screen, 500, 500)
                        occupied[5] = 1
                        winner = True
                    elif turn == 2:
                        drawCircle(pg, screen, 500, 500)
                        occupied[5] = 2
                        winner = True
                #spot 7
                if clickX > 0 and clickX < 200 and clickY > 600 and clickY < 800 and occupied[6] == 0:
                    played = True
                    if turn == 1:
                        drawX(pg, screen, 100, 700)
                        occupied[6] = 1
                        winner = True
                    elif turn == 2:
                        drawCircle(pg, screen, 100, 700)
                        occupied[6] = 2
                        winner = True
                #spot 8
                if clickX > 200 and clickX < 400 and clickY > 600 and clickY < 800 and occupied[7] == 0:
                    played = True
                    if turn == 1:
                        drawX(pg, screen, 300, 700)
                        occupied[7] = 1
                        winner = True
                    elif turn == 2:
                        drawCircle(pg, screen, 300, 700)
                        occupied[7] = 2
                        winner = True
                #spot 9
                if clickX > 400 and clickX < 600 and clickY > 600 and clickY < 800 and occupied[8] == 0:
                    played = True
                    if turn == 1:
                        drawX(pg, screen, 500, 700)
                        occupied[8] = 1
                        winner = True
                    elif turn == 2:
                        drawCircle(pg, screen, 500, 700)
                        occupied[8] = 2
                        winner = True
                if played == True:
                    turn = turnSwap(turn)
                    played = False
            elif winner == True:    
                    if checkwin() == 1:
                        font = pg.font.SysFont('Calibri', 25, False, False)
                        text = font.render("Tie game. Press 'r' to reset", True, [0,0,0])
                        screen.blit(text, [20, 170])
                        pauseGame = True
                        
                        
                    elif checkwin() == 2:
                        font = pg.font.SysFont('Calibri', 25, False, False)
                        text = font.render("Player 1 wins! Press 'r' to reset", True, [0,0,0])
                        screen.blit(text, [20, 170])
                        pauseGame = True
                        
                        
                    elif checkwin() == 3:
                        font = pg.font.SysFont('Calibri', 25, False, False)
                        text = font.render("Player 2 wins! Press 'r' to reset", True, [0,0,0])
                        screen.blit(text, [20, 170])
                        pauseGame = True
            
                    winner = False
            # when r is pressed it resets game
            key = pg.key.get_pressed()
            if key[pg.K_r]:
                screen.fill(bg_color)
                #add the title
                font = pg.font.SysFont('Calibri', 95, False, False)
                text = font.render("Bic Bac Boe", True, [0,0,0])
                screen.blit(text, [100, 40])
                font = pg.font.SysFont('Calibri', 25, False, False)
                pg.draw.rect(screen, [255,255,255], pg.Rect(440, 170, 100, 25), 0)
                text = font.render("Player 1's turn", True, [0,0,0])
                screen.blit(text, [440, 170])
                turn = 1
                occupied = [0,0,0,0,0,0,0,0,0]
                pauseGame = False
        # new bit added May 16th
        # draw mark on hover, to show what would be drawn
        # copy mouse pos tree from above
        hover = pg.mouse.get_pos()
        hoverPosX = hover[0]
        hoverPosY = hover[1]
        skip = 9
        # only if the game is still going
        if pauseGame == False:
            #spot 1
            if hoverPosX > 0 and hoverPosX < 200 and hoverPosY > 200 and hoverPosY < 400 and occupied[0] == 0:
                skip = 0
                if turn == 1:
                    drawHX(pg, screen, 100, 300)
                elif turn == 2:
                    drawHCircle(pg, screen, 100, 300)
            #spot 2
            if hoverPosX > 200 and hoverPosX < 400 and hoverPosY > 200 and hoverPosY < 400 and occupied[1] == 0:
                skip = 1
                if turn == 1:
                    drawHX(pg, screen, 300, 300)
                elif turn == 2:
                    drawHCircle(pg, screen, 300, 300)
            #spot 3
            if hoverPosX > 400 and hoverPosX < 600 and hoverPosY > 200 and hoverPosY < 400 and occupied[2] == 0:
                skip = 2
                if turn == 1:
                    drawHX(pg, screen, 500, 300)
                elif turn == 2:
                    drawHCircle(pg, screen, 500, 300)
            #spot 4
            if hoverPosX > 0 and hoverPosX < 200 and hoverPosY > 400 and hoverPosY < 600 and occupied[3] == 0:
                skip = 3
                if turn == 1:
                    drawHX(pg, screen, 100, 500)
                elif turn == 2:
                    drawHCircle(pg, screen, 100, 500)
            #spot 5
            if hoverPosX > 200 and hoverPosX < 400 and hoverPosY > 400 and hoverPosY < 600 and occupied[4] == 0:
                skip = 4
                if turn == 1:
                    drawHX(pg, screen, 300, 500)
                elif turn == 2:
                    drawHCircle(pg, screen, 300, 500)
            #spot 6
            if hoverPosX > 400 and hoverPosX < 600 and hoverPosY > 400 and hoverPosY < 600 and occupied[5] == 0:
                skip = 5
                if turn == 1:
                    drawHX(pg, screen, 500, 500)
                elif turn == 2:
                    drawHCircle(pg, screen, 500, 500)
            #spot 7
            if hoverPosX > 0 and hoverPosX < 200 and hoverPosY > 600 and hoverPosY < 800 and occupied[6] == 0:
                skip = 6
                if turn == 1:
                    drawHX(pg, screen, 100, 700)
                elif turn == 2:
                    drawHCircle(pg, screen, 100, 700)
            #spot 8
            if hoverPosX > 200 and hoverPosX < 400 and hoverPosY > 600 and hoverPosY < 800 and occupied[7] == 0:
                skip = 7
                if turn == 1:
                    drawHX(pg, screen, 300, 700)
                elif turn == 2:
                    drawHCircle(pg, screen, 300, 700)
            #spot 9
            if hoverPosX > 400 and hoverPosX < 600 and hoverPosY > 600 and hoverPosY < 800 and occupied[8] == 0:
                skip = 8
                if turn == 1:
                    drawHX(pg, screen, 500, 700)
                elif turn == 2:
                    drawHCircle(pg, screen, 500, 700)
                
        # erase over any unoccuppied spots
        for i in range(len(occupied)): 
            if occupied[i] == 0 and skip != i:   
                fillSpot(pg, screen, ((i%3)*200 + 100),(math.floor(i/3)*200 + 300))
        # new function fillSpot created above
        pg.draw.line(screen, [0,0,0], [0, 200], [600, 200], 4)
        pg.draw.line(screen, [0,0,0], [0, 400], [600, 400], 4)
        pg.draw.line(screen, [0,0,0], [0, 600], [600, 600], 4)
        pg.draw.line(screen, [0,0,0], [200, 200], [200, 800], 4)
        pg.draw.line(screen, [0,0,0], [400, 200], [400, 800], 4)        
        
        # end of new code
        
        
        
        pg.display.flip()
        
        clock.tick(60)
    
    pg.quit()
        
if __name__ == "__main__":
    try:
        main()
    except:
        pg.quit()
        raise